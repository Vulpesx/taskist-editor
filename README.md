# TASkist Editor
a tas file editor

<!--toc:start-->
- [TASkist Editor](#taskist-editor)
  - [Description](#description)
  - [Getting Started](#getting-started)
    - [Installation](#installation)
    - [Usage](#usage)
  - [Author](#author)
  - [Version History](#version-history)
  - [License](#license)
<!--toc:end-->

## Description
A shitty editor for tas files,

## Getting Started

### Installation
binaries can be found in Releases

#### cargo
install from cargo with `cargo install taskist-editor`

#### From Source
clone this repo and run `cargo install --path ./taskist-editor`

### Usage
if you installed the binary run that
if you installed with cargo just run `taskist-editor` in you cmd prompt or terminal


## Author
me

## Version History
- 0.1.0
  * the basics

## License
This project is licensed under the [GPLv3](https://www.gnu.org/licenses/gpl-3.0) license
