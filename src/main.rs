use egui_file::FileDialog;
use std::{fs, path::PathBuf};
use tas::*;

use eframe::{
    egui::{self, Slider},
    run_native, App, NativeOptions,
};

mod tas;

enum FileState {
    None,
    Open,
    Save,
}

struct TASEditor {
    file: TasFile,
    path: Option<PathBuf>,
    dialog: Option<FileDialog>,
    file_state: FileState,
}

impl TASEditor {
    fn new() -> Self {
        Self {
            file: TasFile::default(),
            path: None,
            dialog: None,
            file_state: FileState::None,
        }
    }

    fn open_file(&mut self) {
        let mut d = FileDialog::open_file(self.path.to_owned()).show_files_filter(Box::new(|p| {
            if let Some(e) = p.extension() {
                e == "tas"
            } else {
                false
            }
        }));
        d.open();
        self.dialog = Some(d);
        self.file_state = FileState::Open;
    }

    fn save_file(&mut self, new: bool) {
        if new || self.path.is_none() {
            let mut d = FileDialog::save_file(self.path.to_owned())
                .show_files_filter(Box::new(|p| {
                    if let Some(e) = p.extension() {
                        e == "tas"
                    } else {
                        false
                    }
                }))
                .default_filename("NewTas.tas");
            d.open();
            self.dialog = Some(d);
            self.file_state = FileState::Save;
        } else if let Some(p) = self.path.as_ref() {
            fs::write(p, self.file.to_string()).unwrap();
        }
    }

    fn file_dialog(&mut self, ctx: &egui::Context) {
        if let Some(d) = &mut self.dialog {
            match d.show(ctx).state() {
                egui_file::State::Open => {}
                egui_file::State::Closed => self.file_state = FileState::None,
                egui_file::State::Cancelled => self.file_state = FileState::None,
                egui_file::State::Selected => {
                    if let Some(p) = d.path() {
                        match self.file_state {
                            FileState::None => eprintln!("HOW! this should be impossible"),
                            FileState::Open => {
                                self.path = Some(p.into());
                                self.file = TasFile::new(self.path.as_ref().unwrap()).unwrap();
                            }
                            FileState::Save => {
                                fs::write(p, self.file.to_string()).unwrap();
                                self.path = Some(p.into());
                            }
                        }

                        self.file_state = FileState::None;
                    }
                }
            }
        }
    }
}

impl App for TASEditor {
    fn update(&mut self, ctx: &eframe::egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            egui::ScrollArea::vertical().show(ui, |ui| {
                let inputs = self.file.get_inputs_mut();
                let mut min_time = 0.0;
                for i in 0..inputs.len() {
                    let input = &mut inputs[i];
                    ui.horizontal(|ui| {
                        ui.add(
                            egui::DragValue::new(&mut input.time)
                                .clamp_range(min_time..=f32::MAX)
                                .suffix("sec")
                                .speed(0.01),
                        );
                        ui.add(Slider::new(&mut input.steer, -1.0..=1.0));
                        ui.add(Slider::new(&mut input.pitch, -1.0..=1.0));
                        ui.checkbox(&mut input.arms, "arms");
                        ui.checkbox(&mut input.brake, "brake");
                    });
                    min_time = input.time;

                    if ui.button("delete").clicked() {
                        inputs.remove(i);
                        break;
                    }
                }
                ui.horizontal(|ui| {
                    if ui
                        .add_sized(
                            (ui.available_width() / 2.0, 10.0),
                            egui::Button::new("New Input"),
                        )
                        .clicked()
                    {
                        self.file.new_input();
                    }

                    if ui
                        .add_sized(
                            (ui.available_width() / 2.0, 10.0),
                            egui::Button::new("open"),
                        )
                        .clicked()
                        || ui.input(|i| i.key_down(egui::Key::O) && i.modifiers.ctrl)
                    {
                        self.open_file();
                    }

                    if ui
                        .add_sized(
                            (ui.available_width() / 2.0, 10.0),
                            egui::Button::new("save"),
                        )
                        .clicked()
                        || ui.input(|i| i.key_down(egui::Key::S) && i.modifiers.ctrl)
                    {
                        self.save_file(false);
                    }
                    if ui
                        .add_sized((ui.available_width(), 10.0), egui::Button::new("save as"))
                        .clicked()
                        || ui.input(|i| {
                            i.key_down(egui::Key::S) && i.modifiers.ctrl && i.modifiers.shift
                        })
                    {
                        self.save_file(true);
                    }

                    self.file_dialog(ui.ctx());
                });
            })
        });
    }
}

fn main() {
    let options = NativeOptions {
        centered: true,
        ..Default::default()
    };

    run_native(
        "TASkist Editor",
        options,
        Box::new(|_cc| Box::new(TASEditor::new())),
    )
    .unwrap();
}
