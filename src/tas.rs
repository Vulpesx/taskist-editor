use std::{
    fmt::{self, Write},
    fs, io,
    num::ParseFloatError,
    path::PathBuf,
    str::FromStr,
};

use thiserror::Error;

#[derive(Debug, Clone)]
pub struct TasInput {
    pub time: f32,
    pub steer: f32,
    pub pitch: f32,
    pub arms: bool,
    pub brake: bool,
}

impl TasInput {
    fn to_str(&self) -> String {
        let g = |b: bool| if b { "d" } else { "u" };
        format!(
            "{};{};{};{};{};\n",
            self.time,
            self.steer * 100.0,
            self.pitch * 100.0,
            g(self.arms),
            g(self.brake)
        )
    }

    fn from_str(s: &str, last: Self, version: ZeepkistMSV) -> Result<Self, InputError> {
        let mut input = last.clone();
        dbg!(&s);
        for (i, s) in s.split(';').enumerate() {
            let s = s.trim();
            let l = s == ".";
            match (i, version) {
                (0, ZeepkistMSV::V15) => input.time = if l { continue } else { s.parse()? },
                (1, ZeepkistMSV::V15) => {
                    input.steer = if l {
                        continue;
                    } else {
                        (s.parse::<f32>()?) / 100.0
                    }
                }
                (2, ZeepkistMSV::V15) => input.arms = if l { continue } else { s == "d" },
                (3, ZeepkistMSV::V15) => input.brake = if l { continue } else { s == "d" },
                // version 16
                (0, ZeepkistMSV::V16) => input.time = if l { continue } else { s.parse()? },
                (1, ZeepkistMSV::V16) => {
                    input.steer = if l {
                        continue;
                    } else {
                        (s.parse::<f32>()?) / 100.0
                    }
                }
                (2, ZeepkistMSV::V16) => {
                    input.pitch = if l {
                        continue;
                    } else {
                        (s.parse::<f32>()?) / 100.0
                    }
                }
                (3, ZeepkistMSV::V16) => input.arms = if l { continue } else { s == "d" },
                (4, ZeepkistMSV::V16) => input.brake = if l { continue } else { s == "d" },
                _ => break,
            }
        }
        Ok(input)
    }
}

impl fmt::Display for TasInput {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let g = |b: bool| if b { "d" } else { "u" };
        write!(
            f,
            "{};{};{};{};{};",
            self.time,
            self.steer * 100.0,
            self.pitch * 100.0,
            g(self.arms),
            g(self.brake)
        )
    }
}

#[derive(Debug, Error)]
pub enum InputError {
    #[error("{0}")]
    Float(#[from] ParseFloatError),
}

/// minum supported version of zeepkist
#[derive(Clone, Copy)]
pub enum ZeepkistMSV {
    V15,
    V16,
}

impl ZeepkistMSV {
    const LATEST: Self = Self::V16;
}

impl FromStr for ZeepkistMSV {
    type Err = ();

    fn from_str(s: &str) -> std::prelude::v1::Result<Self, Self::Err> {
        match s {
            "v15" => Ok(Self::V15),
            "v16" => Ok(Self::V16),
            _ => Err(()),
        }
    }
}

impl fmt::Display for ZeepkistMSV {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(
            f,
            "{}",
            match self {
                ZeepkistMSV::V15 => "v15",
                ZeepkistMSV::V16 => "v16",
            }
        )
    }
}

impl Default for ZeepkistMSV {
    fn default() -> Self {
        Self::V15
    }
}

#[derive(Debug, Error)]
pub enum TasFileError {
    #[error("{0}")]
    Io(#[from] io::Error),
    #[error("{0}")]
    Input(#[from] InputError),
}

#[derive(Default)]
pub struct TasFile {
    inputs: Vec<TasInput>,
}

impl TasFile {
    pub fn new(p: &PathBuf) -> Result<Self, TasFileError> {
        let mut inputs = vec![];
        let mut last = TasInput::default();

        let mut msv = ZeepkistMSV::default();

        for (i, line) in fs::read_to_string(p)?.lines().enumerate() {
            if i == 0 {
                if let Some((k, v)) = line.trim().split_once('=') {
                    assert_eq!(k, "MSV");
                    if let Ok(version) = ZeepkistMSV::from_str(&v.to_lowercase()) {
                        msv = version;
                    }
                    continue;
                }
            }
            if line.starts_with("//") {
                continue;
            }
            let input = TasInput::from_str(line.trim(), last, msv)?;
            last = input.clone();
            inputs.push(input);
        }

        Ok(Self { inputs })
    }

    pub fn new_input(&mut self) {
        let mut input = TasInput::default();
        if self.len() > 0 {
            input.time = self.get_prev().unwrap().time + 1.0;
        }
        self.inputs.push(input);
    }

    pub fn get_inputs(&self) -> &Vec<TasInput> {
        &self.inputs
    }

    pub fn get_inputs_mut(&mut self) -> &mut Vec<TasInput> {
        &mut self.inputs
    }

    pub fn get_prev(&self) -> Option<&TasInput> {
        self.inputs.last()
    }

    pub fn len(&self) -> usize {
        self.inputs.len()
    }
}

impl fmt::Display for TasFile {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "MSV={}{}",
            ZeepkistMSV::LATEST,
            self.inputs.iter().fold(String::new(), |mut acc, i| {
                let _ = writeln!(acc, "{i}");
                acc
            })
        )
    }
}

impl Default for TasInput {
    fn default() -> Self {
        Self {
            time: 0.0,
            steer: 0.0,
            pitch: 0.0,
            arms: false,
            brake: false,
        }
    }
}
